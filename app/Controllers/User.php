<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\UserModel;
 
class User extends Controller
{
 
    public function index()
    {    
        $model = new UserModel();

        $data = [
            'users' => $model->orderBy('id', 'DESC')->paginate(10),
            'pager' => $model->pager
        ];
        
        return view('user/index', $data);
    }    
 
    public function create()
    {    
        return view('user/add');
    }
 
    public function store()
    {  
 
        helper(['form', 'url']);
         
        $model = new UserModel();
		
        $data = [
            'first_name' => $this->request->getVar('first_name'),
            'last_name'  => $this->request->getVar('last_name'),
            'address'  => $this->request->getVar('address'),
            'email'  => $this->request->getVar('email'),
            'mobile'  => $this->request->getVar('mobile'),
        ];
        $save = $model->insert($data);

        $response['status'] = "ok";
        header('Content-Type: application/json');
        return json_encode($response);
    }
 
    public function edit($id = null)
    {
      
        $model = new UserModel();
 
        $data['user'] = $model->where('id', $id)->first();
 
        return view('user/edit', $data);
    }
 
    public function update()
    {  
 
	    helper(['form', 'url']);
		 
		$model = new UserModel();

		$id = $this->request->getVar('id');

		 $data = [
            'first_name' => $this->request->getVar('first_name'),
            'last_name'  => $this->request->getVar('last_name'),
            'address'  => $this->request->getVar('address'),
            'email'  => $this->request->getVar('email'),
            'mobile'  => $this->request->getVar('mobile'),
		];

		$save = $model->update($id,$data);

        $response['status'] = "ok";
        header('Content-Type: application/json');
        return json_encode($response);
    }
 
    public function delete($id = null){
		$model = new UserModel();
		$data['user'] = $model->where('id', $id)->delete();

        $response['status'] = "ok";
        header('Content-Type: application/json');
        return json_encode($response);
    }
}