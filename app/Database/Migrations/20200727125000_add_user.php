<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUser extends Migration
{

        public function up()
        {
                $this->forge->addField([
                        'id'          => [
                                'type'           => 'INT',
                                'constraint'     => 11,
                                'unsigned'       => true,
                                'auto_increment' => true,
                        ],
                        'first_name'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '100',
                        ],
                        'last_name'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '100',
                        ],
                        'address' => [
                                'type'           => 'TEXT',
                                'null'           => true,
                        ],
                        'email'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '100',
                        ],
                        'mobile'       => [
                                'type'           => 'VARCHAR',
                                'constraint'     => '100',
                        ],
                ]);
                $this->forge->addKey('id', true);
                $this->forge->createTable('users');
        }

        public function down()
        {
                $this->forge->dropTable('users');
        }
}