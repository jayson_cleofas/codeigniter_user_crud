<?= $this->extend('layouts/app') ?>

<?= $this->section('content') ?>

<div class="container mt-4">
    <div class="mb-4 d-flex justify-content-between">
        <h2>Edit User</h2>
        <a href="<?php echo site_url('user') ?>" class="btn btn-dark align-self-center">Back</a>
    </div>

    <form id="edit-user" metho="post" action="<?php echo site_url('user/update');?>">
		<input type="hidden" name="id" class="form-control" id="id" value="<?php echo $user['id'] ?>">
        <div class="form-group row">
            <label for="first_name" class="col-sm-2 col-form-label text-right">First Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $user['first_name'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="last_name" class="col-sm-2 col-form-label text-right">Last Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $user['last_name'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-sm-2 col-form-label text-right">Address</label>
            <div class="col-sm-10">
                <textarea class="form-control" cols="30" rows="3" id="address" name="address"><?php echo $user['address'] ?></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label text-right">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" value="<?php echo $user['email'] ?>">
            </div>
        </div>

        <div class="form-group row">
            <label for="mobile" class="col-sm-2 col-form-label text-right">Mobile</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $user['mobile'] ?>">
            </div>
        </div>

        <div class="form-group row">
        <label for="mobile" class="col-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
$("#edit-user").submit(function(event) {

    event.preventDefault();

    var $form = $(this),
    url = $form.attr('action');
    
    $.ajax({
        method: "POST",
        url: url,
        data: $form.serialize(),
    })
    .done(function( res ) {
        var data = jQuery.parseJSON(res);
        if (data['status'] == 'ok') {
            Swal.fire({
                title: 'Success!',
                text: 'User successfuly updated',
                icon: 'success',
                confirmButtonText: 'Continue'
            })
        }
    });
});
</script>
<?= $this->endSection() ?>