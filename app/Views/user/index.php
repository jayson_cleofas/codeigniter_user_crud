<?= $this->extend('layouts/app') ?>

<?= $this->section('content') ?>

<div class="container mt-4">
	<div class="mb-4 d-flex justify-content-between">
		<h2>Users</h2>
		<a href="<?php echo site_url('user/create') ?>" class="btn btn-primary align-self-center">Add</a>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">Email</th>
				<th scope="col">Mobile</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach($users as $user){
			?>
			<tr>
				<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
				<td><?php echo $user['email']; ?></td>
				<td><?php echo $user['mobile']; ?></td>
				<td><a  href="<?php echo base_url(); ?>/user/edit/<?php echo $user['id']; ?>">Edit</a>&nbsp;<a href="#" data-url="<?php echo base_url(); ?>/user/delete/<?php echo $user['id']; ?>" class="anchor_delete text-danger">Delete</a></td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
	<?= $pager->links() ?>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
$(document).on('click', '.anchor_delete', function (e) {

    e.preventDefault();
    var $this = $(this),
	url = $this.data('url');
    
	const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function ( res ) {
					var data = jQuery.parseJSON(res);
					if (data['status'] == 'ok') location.reload();
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                '',
                'error'
            )
        }
    })
});
</script>
<?= $this->endSection() ?>